#
# THIS FILE IS GENERATED, DO NOT EDIT

################################################################################
#
# Ubuntu checks
#
################################################################################

#
# Common variable definitions
#
.ci-commons-ubuntu:
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    FDO_DISTRIBUTION_EXEC: '/bin/bash test/script.sh'
    FDO_DISTRIBUTION_VERSION: '19.10'
    FDO_DISTRIBUTION_TAG: fdo-ci-$CI_PIPELINE_ID
    FDO_EXPIRES_AFTER: '1h'
  needs:
    - bootstrap
    - sanity check

#
# A few templates to avoid writing the image and stage in each job
#
.ubuntu:ci@container-build:
  extends:
    - .fdo.container-build@ubuntu
    - .ci-commons-ubuntu
  image: $CI_REGISTRY_IMAGE/buildah:2020-03-24
  stage: ubuntu_container_build


.ubuntu:ci@container-build@arm64v8:
  extends:
    - .fdo.container-build@ubuntu@arm64v8
    - .ci-commons-ubuntu
  image: $CI_REGISTRY_IMAGE/arm64v8/buildah:2020-03-24
  stage: ubuntu_container_build
  needs:
    - bootstrap@arm64v8
    - sanity check


#
# Qemu build
#
.ubuntu:ci@qemu-build:
  extends:
    - .fdo.qemu-build@ubuntu
    - .ci-commons-ubuntu
  image: $CI_REGISTRY_IMAGE/qemu-mkosi-base:2020-03-24
  stage: ubuntu_container_build
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out
  needs:
    - bootstrap-qemu-mkosi
    - sanity check

#
# generic ubuntu checks
#
.ubuntu@check:
  extends:
    - .fdo.distribution-image@ubuntu
    - .ci-commons-ubuntu
  stage: ubuntu_check
  script:
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - curl --insecure https://gitlab.freedesktop.org
    - wget --no-check-certificate https://gitlab.freedesktop.org
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo $FDO_DISTRIBUTION_EXEC properly run ;
      else
        exit 1 ;
      fi


.ubuntu@qemu-check:
  stage: ubuntu_check
  extends:
    - .fdo.distribution-image@ubuntu
    - .ci-commons-ubuntu
  tags:
    - kvm
  script:
    - pushd /app
      # start the VM
    - bash /app/vmctl start
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - /app/vmctl exec curl --insecure https://gitlab.freedesktop.org
    - /app/vmctl exec wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - bash /app/vmctl stop

      # start the VM, with the kernel parameters
    - bash /app/vmctl start-kernel
      # make sure we can still use curl/wget
    - /app/vmctl exec curl --insecure https://gitlab.freedesktop.org
    - /app/vmctl exec wget --no-check-certificate https://gitlab.freedesktop.org
      # terminate the VM
    - bash /app/vmctl stop
  artifacts:
    name: logs-$CI_PIPELINE_ID
    when: always
    expire_in: 1 week
    paths:
      - console.out


#
# straight ubuntu build and test
#
ubuntu:19.10@container-build:
  extends: .ubuntu:ci@container-build


ubuntu:19.10@check:
  extends: .ubuntu@check
  needs:
    - ubuntu:19.10@container-build
    - sanity check

# Test FDO_BASE_IMAGE. We don't need to do much here, if our
# FDO_DISTRIBUTION_EXEC script can run curl+wget this means we're running on
# the desired base image. That's good enough.
ubuntu:19.10@base-image:
  extends: ubuntu:19.10@container-build
  stage: ubuntu_check
  variables:
    # We need to duplicate FDO_DISTRIBUTION_TAG here, gitlab doesn't allow nested expansion
    FDO_BASE_IMAGE: registry.freedesktop.org/$CI_PROJECT_PATH/ubuntu/19.10:fdo-ci-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: ''
    FDO_DISTRIBUTION_EXEC: 'test/test-wget-curl.sh'
    FDO_FORCE_REBUILD: 1
    FDO_DISTRIBUTION_TAG: fdo-ci-baseimage-$CI_PIPELINE_ID
  needs:
    - ubuntu:19.10@container-build
    - sanity check

#
# /cache ubuntu check (in build stage)
#
# Also ensures setting FDO_FORCE_REBUILD will do the correct job
#
ubuntu@cache-container-build:
  extends: .ubuntu:ci@container-build
  before_script:
      # The template normally symlinks the /cache
      # folder, but we want a fresh new one for the
      # tests.
    - mkdir runner_cache_$CI_PIPELINE_ID
    - uname -a | tee runner_cache_$CI_PIPELINE_ID/foo-$CI_PIPELINE_ID

  artifacts:
    paths:
      - runner_cache_$CI_PIPELINE_ID/*
    expire_in: 1 week

  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-cache-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_EXEC: 'bash test/test_cache.sh $CI_PIPELINE_ID'
    FDO_CACHE_DIR: $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID
    FDO_FORCE_REBUILD: 1

#
# /cache ubuntu check (in check stage)
#
ubuntu@cache-check:
  stage: ubuntu_check
  image: alpine:latest
  script:
    # in the previous stage (ubuntu@cache-container-build),
    # test/test_cache.sh checked for the existance of `/cache/foo-$CI_PIPELINE_ID`
    # and if it found it, it wrote `/cache/bar-$CI_PIPELINE_ID`.
    #
    # So if we have in the artifacts `bar-$CI_PIPELINE_ID`, that means
    # 2 things:
    # - /cache was properly mounted while building the container
    # - the $FDO_CACHE_DIR has been properly written from within the
    #   building container, meaning the /cache folder has been successfully
    #   updated.
    - if [ -e $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID/bar-$CI_PIPELINE_ID ] ;
      then
        echo Successfully read/wrote the cache folder, all good ;
      else
        echo FAILURE while retrieving the previous artifacts ;
        exit 1 ;
      fi
  needs:
    - job: ubuntu@cache-container-build
      artifacts: true
    - sanity check


ubuntu:19.10@container-build@arm64v8:
  extends: .ubuntu:ci@container-build@arm64v8
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-arm64v8-$CI_PIPELINE_ID


ubuntu:19.10@check@arm64v8:
  extends: .ubuntu@check
  tags:
    - aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-arm64v8-$CI_PIPELINE_ID
  needs:
    - ubuntu:19.10@container-build@arm64v8
    - sanity check


ubuntu:19.10@qemu-build:
  extends: .ubuntu:ci@qemu-build
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-qemu-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    QEMU_BASE_IMAGE: $CI_REGISTRY_IMAGE/qemu-base:2020-03-24


ubuntu:19.10@qemu-check:
  extends: .ubuntu@qemu-check
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-qemu-$CI_PIPELINE_ID
  needs:
    - bootstrap-qemu-mkosi
    - ubuntu:19.10@qemu-build
    - sanity check


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
do not rebuild ubuntu:19.10@container-build:
  extends: .ubuntu:ci@container-build
  stage: ubuntu_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - ubuntu:19.10@container-build
    - sanity check


#
# check if the labels were correctly applied
#
check labels ubuntu:19.10:
  extends:
    - ubuntu:19.10@check
  image: $CI_REGISTRY_IMAGE/buildah:2020-03-24
  script:
    # FDO_DISTRIBUTION_IMAGE still has indirections
    - DISTRO_IMAGE=$(eval echo ${FDO_DISTRIBUTION_IMAGE})

    # retrieve the infos from the registry (once)
    - JSON_IMAGE=$(skopeo inspect docker://$DISTRO_IMAGE)

    # parse all the labels we care about
    - IMAGE_PIPELINE_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.pipeline_id"]')
    - IMAGE_JOB_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.job_id"]')
    - IMAGE_PROJECT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.project"]')
    - IMAGE_COMMIT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.commit"]')

    # some debug information
    - echo $JSON_IMAGE
    - echo $IMAGE_PIPELINE_ID $CI_PIPELINE_ID
    - echo $IMAGE_JOB_ID
    - echo $IMAGE_PROJECT $CI_PROJECT_PATH
    - echo $IMAGE_COMMIT $CI_COMMIT_SHA

    # ensure the labels are correct (we are on the same pipeline)
    - '[[ x"$IMAGE_PIPELINE_ID" == x"$CI_PIPELINE_ID" ]]'
    - '[[ x"$IMAGE_JOB_ID" != x"" ]]' # we don't know the job ID, but it must be set
    - '[[ x"$IMAGE_PROJECT" == x"$CI_PROJECT_PATH" ]]'
    - '[[ x"$IMAGE_COMMIT" == x"$CI_COMMIT_SHA" ]]'
  needs:
    - ubuntu:19.10@container-build
    - sanity check


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where FDO_REPO_SUFFIX == ci_templates_test_upstream
#
pull upstream ubuntu:19.10@container-build:
  extends: .ubuntu:ci@container-build
  stage: ubuntu_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_REPO_SUFFIX: ubuntu/ci_templates_test_upstream
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - ubuntu:19.10@container-build
    - sanity check

#
# Try our ubuntu scripts with other versions and check
#

ubuntu:18.04@container-build:
  extends: .ubuntu:ci@container-build
  variables:
    FDO_DISTRIBUTION_VERSION: '18.04'

ubuntu:18.04@check:
  extends: .ubuntu@check
  variables:
    FDO_DISTRIBUTION_VERSION: '18.04'
  needs:
    - ubuntu:18.04@container-build
    - sanity check

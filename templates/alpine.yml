#
# THIS FILE IS GENERATED, DO NOT EDIT


# we cannot reuse exported variables in after_script,
# so let's have a common definition
.fdo.alpine_vars: &fdo_distro_vars
    # exporting templates variables
    # https://gitlab.com/gitlab-com/support-forum/issues/4349
  - export BUILDAH_FORMAT=docker

  - export DISTRO=alpine

  - export DISTRO_TAG=$FDO_DISTRIBUTION_TAG
  - export FDO_DISTRIBUTION_VERSION="latest"
  - export DISTRO_VERSION="$FDO_DISTRIBUTION_VERSION"
  - export DISTRO_EXEC=$FDO_DISTRIBUTION_EXEC

  - if [ x"$FDO_REPO_SUFFIX" == x"" ] ;
    then
            export FDO_REPO_SUFFIX=alpine/$DISTRO_VERSION ;
    fi

  - export BUILDAH_RUN="buildah run --isolation chroot"
  - export BUILDAH_COMMIT="buildah commit --format docker"
  - export CACHE_DIR=${FDO_CACHE_DIR:-/cache}

    # to be able to test the following script in the CI of the ci-templates
    # project itself, we need to put a special case here to have a
    # different image to pull if it already exists
  - export REPO_SUFFIX_LOCAL=$FDO_REPO_SUFFIX
  - if [[ x"$REPO_SUFFIX_LOCAL" == x"alpine/ci_templates_test_upstream" ]] ;
    then
            export FDO_REPO_SUFFIX=${DISTRO}/${DISTRO_VERSION} ;
    fi

# have a special definition for the test if the image already exists
.fdo.alpine_exists: &fdo_distro_exists |
  # if-not-exists steps
  set -x

  if [[ -z "$FDO_FORCE_REBUILD" ]]
  then
    # disable exit on failure
    set +e

    # check if our image is already in the current registry
    # we store the sha of the digest and the layers
    skopeo inspect docker://$CI_REGISTRY_IMAGE/$REPO_SUFFIX_LOCAL:$DISTRO_TAG | jq '[.Digest, .Layers]' > local_sha

    # check if our image is already in the upstream registry
    if [[ -z "$FDO_UPSTREAM_REPO" ]]
    then
      echo "WARNING! Variable \$FDO_UPSTREAM_REPO is undefined, cannot check for images"
    else
      skopeo inspect docker://$CI_REGISTRY/$FDO_UPSTREAM_REPO/$FDO_REPO_SUFFIX:$DISTRO_TAG | jq '[.Digest, .Layers]' > upstream_sha
    fi

    # reenable exit on failure
    set -e

    # if the upstream repo has an image, ensure we use the same
    if [ -s upstream_sha ]
    then

      # ensure we use the same image from upstream
      diff upstream_sha local_sha && exit 0 || true

      # copy the original image into the current project registry namespace
      # we do 2 attempts with skopeo copy at most
      skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                  docker://$CI_REGISTRY/$FDO_UPSTREAM_REPO/$FDO_REPO_SUFFIX:$DISTRO_TAG \
                  docker://$CI_REGISTRY_IMAGE/$REPO_SUFFIX_LOCAL:$DISTRO_TAG || \
      skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                  docker://$CI_REGISTRY/$FDO_UPSTREAM_REPO/$FDO_REPO_SUFFIX:$DISTRO_TAG \
                  docker://$CI_REGISTRY_IMAGE/$REPO_SUFFIX_LOCAL:$DISTRO_TAG

      exit 0
    fi

    # if we have a local image but none in the upstream repo, use our
    if [ -s local_sha ]
    then
      exit 0
    fi
  fi

.fdo.alpine:
  variables:
    FDO_DISTRIBUTION_NAME: "alpine"

###
# Checks for a pre-existing alpine container image and builds it if
# it does not yet exist.
#
# If an image with the same version or suffix exists in the upstream project's registry,
# the image is copied into this project's registry.
# If no such image exists, the image is built and pushed to the local registry.
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-alpine-image:
#       extends: .fdo.container-build@alpine
#       variables:
#          FDO_DISTRIBUTION_PACKAGES: 'curl wget gcc valgrind'
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#
#
#
# **Reserved by this template:**
#
# - ``image:`` do not override
# - ``script:`` do not override
#
# **Variables:**
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
# .. attribute:: FDO_UPSTREAM_REPO
#
#       The GitLab project path to the upstream project
#
# .. attribute:: FDO_REPO_SUFFIX
#
#       The repository name suffix to use, see below.
#
# .. attribute:: FDO_DISTRIBUTION_PACKAGES
#
#       Packages to install as a space-separated single string, e.g. "curl wget"
#
# .. attribute:: FDO_DISTRIBUTION_EXEC
#
#       An executable run after the installation of the :attr:`FDO_DISTRIBUTION_PACKAGES`
#
# .. attribute:: FDO_FORCE_REBUILD
#
#       If set, the image will be built even if it exists in the registry already
#
# .. attribute:: FDO_BASE_IMAGE
#
#       By default, the base image to start with is
#       ``alpine:latest``
#       and all dependencies are installed on top of that base image. If
#       ``FDO_BASE_IMAGE`` is given, it references a different base image to start with.
#       This image usually requires the full registry path, e.g.
#       ``registry.freedesktop.org/projectGroup/projectName/repo_suffix:tag-name``
#
# .. attribute:: FDO_EXPIRES_AFTER
#
#       If set, enables an expiration time on the image to
#       aid the garbage collector in deciding when an image can be removed. This
#       should be set for temporary images that are not intended to be kept
#       forever. Allowed values are e.g. ``1h`` (one hour), ``2d`` (two days) or
#       ``3w`` (three weeks).
#
# .. attribute:: FDO_CACHE_DIR
#
#       If set, the given directory is mounted as ``/cache``
#       when ``FDO_DISTRIBUTION_EXEC`` is run. This can allow for passing of
#       cache values between build jobs (if run on the same runner).  You should
#       not usually need to set this, it defaults to ``/cache`` from the host
#       and thus enables cache sharing by default.
#
# The resulting image will be pushed to the local registry.
#
# If :attr:`FDO_REPO_SUFFIX` was specified, the image path is
# ``$CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG``.
# Use the ``.fdo.suffixed-image@alpine`` template to access or use this image.
#
# If :attr:`FDO_REPO_SUFFIX` was **not** specified, the image path is
# ``$CI_REGISTRY_IMAGE/alpine/latest:$FDO_DISTRIBUTION_TAG``.
# Use the ``.fdo.distribution-image@alpine`` template to access or use this image.
#
.fdo.container-build@alpine:
  extends: .fdo.alpine
  image: $CI_REGISTRY/freedesktop/ci-templates/buildah:2020-03-24
  stage: build
  script:
    # log in to the registry
  - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

  - *fdo_distro_vars
  - if [[ x"$DISTRO_TAG" == x"" ]] ;
    then
      echo $DISTRO tag missing;
      exit 1;
    fi

  - *fdo_distro_exists

  - if [[ x"$FDO_BASE_IMAGE" != x"" ]] ;
    then
      BASE_IMAGE="$FDO_BASE_IMAGE";
    else
      BASE_IMAGE="alpine:latest";
    fi
  - echo Building $FDO_REPO_SUFFIX:$DISTRO_TAG from $BASE_IMAGE
    # initial set up: take the base image, update it and install the packages
  - buildcntr=$(buildah from $BASE_IMAGE)
  - buildmnt=$(buildah mount $buildcntr)

  - $BUILDAH_RUN $buildcntr apk update
  - $BUILDAH_RUN $buildcntr apk upgrade
  - $BUILDAH_RUN $buildcntr apk add bash

  - if [[ x"$FDO_DISTRIBUTION_PACKAGES" != x"" ]] ;
    then
      $BUILDAH_RUN $buildcntr apk add $FDO_DISTRIBUTION_PACKAGES ;
    fi

    # check if there is an optional post install script and run it
  - if [[ x"$DISTRO_EXEC" != x"" ]] ;
    then
      echo Running $DISTRO_EXEC ;
      set -x ;
      mkdir $buildmnt/tmp/clone ;
      pushd $buildmnt/tmp/clone ;
      git init ;
      git remote add origin $CI_REPOSITORY_URL ;
      git fetch --depth 1 origin $CI_COMMIT_SHA ;
      git checkout FETCH_HEAD  > /dev/null;
      buildah config --workingdir /tmp/clone --env HOME="$HOME" $buildcntr ;
      if [ -e $CACHE_DIR ] ;
      then
        CACHE="-v $CACHE_DIR:/cache:rw,shared,z" ;
      fi ;
      $BUILDAH_RUN $CACHE $buildcntr bash -c "set -x ; $DISTRO_EXEC" ;
      popd ;
      buildah config --workingdir / --env HOME="$HOME" $buildcntr ;
      rm -rf $buildmnt/tmp/clone ;
      set +x ;
    fi

    # do not store the packages database, it's pointless
  - $BUILDAH_RUN $buildcntr sh -c 'rm -f /var/cache/apk/APKINDEX.*'

    # set up the working directory
  - mkdir -p $buildmnt/app
  - buildah config --workingdir /app $buildcntr
    # umount the container, not required, but, heh
  - buildah unmount $buildcntr
  - if [[ x"$FDO_EXPIRES_AFTER" != x"" ]] ;
    then
      buildah config -l fdo.expires-after=$FDO_EXPIRES_AFTER $buildcntr;
    fi

  - if [[ x"$FDO_UPSTREAM_REPO" != x"" ]] ;
    then
      buildah config -l fdo.upstream-repo=$FDO_UPSTREAM_REPO $buildcntr;
    fi

  - buildah config -l fdo.pipeline_id="$CI_PIPELINE_ID" $buildcntr;
  - buildah config -l fdo.job_id="$CI_JOB_ID" $buildcntr;
  - buildah config -l fdo.project="$CI_PROJECT_PATH" $buildcntr;
  - buildah config -l fdo.commit="$CI_COMMIT_SHA" $buildcntr;

    # tag the current container
  - $BUILDAH_COMMIT $buildcntr $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG

    # clean up the working container
  - buildah rm $buildcntr

    # push the container image to the registry
    # There is a bug when pushing 2 tags in the same repo with the same base:
    # this may fail. Just retry it after.
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$DISTRO_TAG

###
# Checks for a pre-existing alpine container image for the
# ``arm64v8`` processor architecture and builds it if it does not yet exist.
#
# This template requires runners with the ``aarch64`` tag.
#
# See ``.fdo.container-build@alpine`` for details.
.fdo.container-build@alpine@arm64v8:
  extends: .fdo.container-build@alpine
  image: $CI_REGISTRY/freedesktop/ci-templates/arm64v8/buildah:2020-03-24
  tags:
    - aarch64


###
# alpine template that pulls the alpine image from the
# registry based on ``FDO_DISTRIBUTION_TAG``.
# This template must be provided the same variable values as supplied in
# ``.fdo.container-build@alpine``.
#
# This template sets ``image:`` to the generated image. You may override this.
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-alpine-test:
#       extends: .fdo.distribution-image@alpine
#       variables:
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#       script:
#         - meson builddir
#         - ninja -C builddir test
#
# **Variables:**
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
#       The value supplied must be the same as supplied in ``.fdo.container-build@alpine``.
#
# .. attribute:: FDO_DISTRIBUTION_IMAGE
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Path to the registry image
#
# .. attribute:: FDO_DISTRIBUTION_NAME
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Set to the string "alpine"
#
# .. note:: If you used ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.suffixed-image@alpine`` instead.
.fdo.distribution-image@alpine:
  extends: .fdo.alpine
  image: $CI_REGISTRY_IMAGE/alpine/latest:$FDO_DISTRIBUTION_TAG
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/alpine/latest:$FDO_DISTRIBUTION_TAG

###
# alpine template that pulls the alpine image from the
# registry based on ``FDO_REPO_SUFFIX``.
# This template must be provided the same variable values as supplied in
# ``.fdo.container-build@alpine``.
#
# This template sets ``image:`` to the generated image. You may override this.
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-alpine-test:
#       extends: .fdo.distribution-image@alpine
#       variables:
#          FDO_REPO_SUFFIX: 'some/path'
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#       script:
#         - meson builddir
#         - ninja -C builddir test
#
#
# **Variables:**
#
# .. attribute:: FDO_REPO_SUFFIX
#
#       **This variable is required**
#
#       The repository name suffix.
#
#       The value supplied must be the same as supplied in ``.fdo.container-build@alpine``.
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
#       The value supplied must be the same as supplied in ``.fdo.container-build@alpine``.
#
# .. attribute:: FDO_DISTRIBUTION_IMAGE
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Path to the registry image
#
# .. attribute:: FDO_DISTRIBUTION_NAME
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Set to the string "alpine"
#
#
# Variables provided by this template should be considered read-only.
#
# .. note:: If you did not use ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.distribution-image@alpine`` instead.
.fdo.suffixed-image@alpine:
  extends: .fdo.alpine
  image: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG